<?php

/**
 * @file
 * Defines admin page callbacks.
 *
 * Copyright (C) 2014, Victor Nikulshin
 *
 * This file is part of 'Node Weight Order' Drupal module.
 *
 * Node Weight Order is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Node Weight Order is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Node Weight Order.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Taxonomy term group ordering.
 */
function node_weight_order_taxonomy_weight_page($term) {
  drupal_set_title('Изменение порядка в группе «' . $term->name . '»');

  $group_id = $term->vocabulary_machine_name . '-' . $term->tid;
  $weights = node_weight_order_load_group_weights($group_id);

  return drupal_get_form('node_weight_order_reorder_form', $group_id, $weights);
}
