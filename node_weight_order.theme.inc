<?php

/**
 * @file
 * Implements Drupal theme hooks.
 *
 * Copyright (C) 2014, Victor Nikulshin
 *
 * This file is part of 'Node Weight Order' Drupal module.
 *
 * Node Weight Order is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Node Weight Order is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Node Weight Order.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Theming hook for node sorting form.
 */
function theme_node_weight_order_reorder_form($variables) {
  $form = $variables['form'];
  $output = '';

  drupal_add_tabledrag('node-weight-order-table', 'order', 'self', 'weight');

  $header = array(
    'Заголовок', 'Дата добавления', 'Порядок',
  );

  $rows = array();
  foreach (element_children($form['items']) as $item) {
    if ($form['items'][$item]['title']['#sticky_flag']) {
      $sticky_markup = ' <em>(закреплено)</em>';
    }
    else {
      $sticky_markup = '';
    }
    $rows[] = array(
      'data' => array(
        drupal_render($form['items'][$item]['title']) . $sticky_markup,
        drupal_render($form['items'][$item]['created']),
        drupal_render($form['items'][$item]['weight']),
      ),
      'class' => array('draggable'),
    );
  }

  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'node-weight-order-table')));
  $output .= drupal_render_children($form);
  return $output;
}
